# otu_shape


## Name

otu_shape

## Brief Description

Provides a tool for assessing the intrinsec quality of OTUs built in metabarcoding, as a two steps procedure: 
* how to learn a SVM classifier on a training set, to classify a single OTU as with/without noise 
* knowing that, how to type an OTU in a sample, not in the training set, as composed or single, and if single, as with or without noise.

This git is a companion to manuscript   
Marie-Josée Cros, Jean-Marc Frigerio, Nathalie Peyrard and Alain Franc (202x) _Simple approaches for evaluation of OTU quality based on dissimilarity arrays_, submitted to Metabarcoding and Metagenomics (MBMG).   

## Installation

This project can be installed via a `git clone`, as follows

* Select a location in your laptop where you want to install the project, the location being the parent directory
* go into this parent directory 
* type 

 
`git clone git@forgemia.inra.fr:alain.franc/otu_shape.git`

A directory `otu_shape` is created, containing all the subdirectories and files provided by the project (replicability, tutorial, readme). 


## Usage and dependencies

See the tutorial, and the documentation in directory **replicability** for further details on arguments.

The project has been written for laptops or servers under `Linux Ubuntu`.

The following softwares and dependencies have to be installed:
* `python 3.8` or higher 
* python libraries `numpy`, `matplotib` and `scikit-learn`
* `R 4.2` or higher 
* R library `blockmodels` 

If the library `blockmodels` is not installed on the user's computer, it will be installed on the fly. Indeed, program `learn_noise_clasifier.py` will trigger a R-script which tests whether `blockmodels`is installed or not, and, if not, will install it. 


## Organisation of the repository

The repository is organised with two main directories:  
* one for replicability of the study in the manuscript, named **replicability**
* a tutorial for an easy accessibility to the codes, with a real dataset smaller than the one analysed in the manuscript, named **tutorial**. 



## Replicability

This directory contains the codes used for the numerical experiments in manuscript Cros & al., 202x, and a documentation for the codes. The codes are provided here for accessibility and replicability of the numerical experiments. However, the size of the data set been too large, it is only available on the FigShare project https://doi.org/10.6084/m9.figshare.20764690.v3. The user can download the dataset in his/her own laptop, knowing that 80 Go of RAM are necessary for some samples.


This directory provides a way to replicate the procedure used for producing the article's results. It is organised with:
* a directory `SRC` with the codes used for the numerical experiments in manuscript Cros & al., 202x,
* a documentation for the codes (`documentation_for_replication.pdf`)
* a directory `OUTPUT_DIATOMS` with all the results on the data sets used in the manuscript, for both the training set (Teychan) and the samples to be typed (other locations). 

**note:** Replicability can be done as well using the codes and the datasets provided in the figshare project.


## Tutorial

As the data used in the manuscript represent a significant volume (> 6  Go), a tutorial on how to use the programs with smaller "toy" datasets is  provided. The programs used for this tutorial are the same as the original ones, and have been copied in the tutorial to facilitate if useful their deployment and evolution (this tutorial is a benchmark as well for the possible evolution of the programs). For example, the management of I/O directories for learning and typing has been changed, for a better distinction between learning and typing. Some comments on the progress of the calculation have been added too. 

#### -> datasets for the tutorial 

The origin of the significant memory occupancy of the datasets is in the first OTUs of each sample (OTUs are ranked by decreasing size): it is common that the first OTU is composed of more than ten thousands reads, and occupies several hundreds of Mo of memory (see appendix). For the tutorial, we have removed these OTUs (those who contain more than 1,000 reads), as well as those which are smaller than 20 OTUs. All other OTUs in a sample have been kept. For learning the SVM classifier, OTUs from Teychan, four seasons, and pelagic only have been kept in the training set. 


The dataset used in the manuscript is made of two sets
* a set of 4 samples (Teychan, pelagic, four seasons), forming the training set and used for learning the SVM classifier, 
* a set of one sample (Jaquets, pelagic, automn) used as an example. The goal is to type each OTU of this sample as composed, single with or without noise.

Each sample is given as a family of dissimilarity arrays, one per OTU. 

#### -> organisation

The tutorial is run in two steps:  
* a first step to learn the SVM classifier, 
* a second step to type the OTUs in a sample.  


Each step is made of 
* one or several input directorie(s)
* one program (and only one, even if it calls other programs) to be run
* one output directory with results.

**learning:** These items are
* input directory: `data_for_learning`
* program: `learn_noise_classifier.py` 
* output directory: `results_from_learning`

**typing** These items are
* input directories: `data_for_typing`, `results_from_learning`
* program: `type_otu.py` 
* output directory: `results_from_typing`

The classifier learnt in learning phase is written as a file in directory `results_from_learning`, and is read by program `type_otu.py`. Hence this directory is both an output of learning program and an input of typing program.

#### -> to learn the classifier

The input directory `data_for_learning` is subdivided into 4 subdirectories, one per sample: `Tey_pel_spring`, `Tey_pel_summer`, `Tey_pel_autumn` and `Tey_pel_winter`.  Each subdirectory contains a list of dissimilarity arrays, one per OTU, identical to the one used for the manuscript.    

Here is the procedure for learning the classifier:
* go into directory `tutorial` 
* type the command `./learn_noise_classifier.py -d data_for_learning -o results_from_learning Tey_pel_spring Tey_pel_summer Tey_pel_autumn Tey_pel_winter` 

The arguments are as follows: 
- `-d data_for_learning`: all samples used for learning the SVM classifier are located in subdirectories of directory `data_for_learning`  
- `-o results_from_learning`: all results about the SVM classifier are written in subdirectory `results_from_learning`  
-  `Tey_pel_spring Tey_pel_summer Tey_pel_autumn Tey_pel_winter` are the names of the four samples selected for learning, and are as well the names of as many subdirectories of directory `data_for_learning`.    


Note that all the subdirectories have been created when cloning the git repository, and the four datasets have been loaded. So, normally, it should work swiftly.   

The outputs are written in directory `results_from_learning`, and detailed below.

#### -> To interpret the results

All results are available as files, written in directory `tutorial/results_from_learning`. They are 

**params_noise_classifier:** contains the equation of the line in feature space to separate with noised and without noise OTUs. This file will be read for typing new samples.  

**params_noise_classifier.txt:** contains the confusion matrix of the typing on training set and the mean AUC with 10 cross-validations.

**params_noise_classifier.png:** plot of the OTUs of the training set colored according to their classification by expertise, and located in the feature space. It should look like 

![theta.png](tutorial/params_noise_classifier.png){width=250px, height=250px}

**Tey_pel_&lt;season&gt;_theta.png:** for each season (and the sample corresponding to this season), the plot of the Kernel Density Estimator of the parameter $\theta$ and the location of its critical value for typing single or composed.  

**training_set_theta.png:** the same plot for all seasons together (the whole traing set).
 
#### -> to type a new sample

Once the SVM classifier has been learned, it can be used to type all the OTUs of a new sample. This must be done at the scale of a sample, because typing as composed / simple is done at the scale of a sample. Typing is done sample per sample. For example, to type sample `Jac_pel_automn`, 

* be in directory `tutorial`
* type `./type_otu.py  -d  data_for_typing -l results_from_learning  -o results_from_typing Jac_pel_autumn`   


The arguments are as follows: 
* `-d  data_for_typing`: directory where datasets of sample to type are located; the dissimilarity arrays of the sample are read in a subdirectory of `data_for_typing`; four are given and can be called: `Jac_pel_autumn`, `Jac_pel_spring`, `Jac_pel_summer`, `Jac_pel_winter` 
* `-l results_from_learning` : this directory contains the file where the classifier has been written in learning phase, and must be given as a second input 
* `-o results_from_typing`: output directory, where the results are written in files.
*  `Jac_pel_autumn`: the sample to be typed; there must be a subdirectory with same name in directory `data_for_typing`


All results are available as files, written in directory `tutorial/results_from_typing`. They are  

**Jac_pel_autumn_theta.png:** the plot of the Kernel Density Estimator of the parameter $\theta$ and the location of its critical value for typing single or composed.  

**Jac_pel_autumn_type.txt:** a table, which can be opened with a spreadsheet, with one row per OTU and the following columns:
* `otu`: number of the OTU
* `#sequences`: number of reads
* `is_composed`: whether it is composed (=1) or single (=0)
* `is_noisy`: whether it is noisy (=1) or not (=0), and -1 for composed OTUs
* `theta`: value of $\theta$ for the OTU
* `y`: the value returned by the classifier: with noise if > 0, without noise if < 0  
* `degree20`: fraction of nodes (=reads) with degree < 0.2, scaled


## Support

In case of need for support, please contact
* Alain Franc ; alain.franc@inrae.fr
* Nathalie Peyrard ; nathalie.peyrard@inrae.fr
* Jean-Marc Frigerio ; jean-marc.frigerio@inrae.fr


## Contributing

If you feel like contributing, please contact
* Alain Franc ; alain.franc@inrae.fr
* Nathalie Peyrard ; nathalie.peyrard@inrae.fr

## Data accessibility

The codes for learning the noise classifier, for determining the type of OTUs, and the dataset used for our study are available on the Figshare project (Cros et al., 2022). The dissimilarity arrays are publicly available as well on Recherche Data Gouv (Auby & al., 2022, Malabar project).

Auby, Isabelle; Méteigner, Claire; Rumebe, Myriam; Chancerel, Emilie; Salin, Franck; Aluome, Christelle; Barraquand, Frédéric; Carassou, Laure; Del Amo, Yolanda; Meleder, Vona; Petit, Alexandra; Picoche, Coralie; Frigerio, Jean-Marc; Franc, Alain, 2022, "Malabar datasets used in study "OTU quality from dissimilarity arrays", https://doi.org/10.57745/7T2UCB, Recherche Data Gouv, V1 

Cros, M.-J., Frigerio, J.-M., Peyrard, N., and Franc, A. (2022). Code, dataset and results for the study ”OTU quality from dissimilarity arrays”. Figshare, https://doi.org/10.6084/m9.figshare.20764690.v3. 

## Appendix

Here are for each OTU with dissimilarity array of size > 1 Go  
* the number of reads of the OTU, 
* the size in octets of the dissimilarity array.
 

| File and large OTU(s)                | Nb reads |         Size   |
| -------------------------------------| --------:| --------------:|
|DATA_DIATOMS/B13_BEN_Winter/otu_0.dis |   15,354 |   1,221,160,587|     
|DATA_DIATOMS/B13_PEL_Autumn/otu_0.dis |   19,674 |   2,007,572,131|    
|DATA_DIATOMS/B13_PEL_Spring/otu_0.dis |   17,756 |   1,621,842,502|
|DATA_DIATOMS/B13_PEL_Summer/otu_0.dis |   16,753 |   1,442,598,079|
|DATA_DIATOMS/COM_PEL_Autumn/otu_0.dis |   16,695 |   1,314,462,738|
|DATA_DIATOMS/COM_PEL_Summer/otu_0.dis |   25,403 |   3,261,493,725|
|DATA_DIATOMS/JAC_PEL_Autumn/otu_0.dis |   22,203 |   2,548,015,655|
|DATA_DIATOMS/JAC_PEL_Spring/otu_0.dis |   25,904 |   3,465,752,252|
|DATA_DIATOMS/TEY_PEL_Autumn/otu_0.dis |   18,541 |   1,719,156,549|
|DATA_DIATOMS/TEY_PEL_Spring/otu_0.dis |   17,165 |   1,509,393,166|






## Authors and acknowledgment

Authors are 
* Marie-Josée Cros, who has written the codes for the manuscript, available in directory `replicability`, and the documentation **documentation_for_replicability**
* Alain Franc, Jean-Marc Frigerio, and Nathalie Peyrard who designed this gitlab and developed the tutorial 

## License

These codes are available with GPL-3.0 and later licence.

## Project status

On going project.  
version 0.0.1 ; November 23rd, 2023.
