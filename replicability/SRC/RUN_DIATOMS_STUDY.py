#!/bin/sh

# Run the diatoms study (see UserDocumentation.pdf for detail)
# Beware that due to big OTUs, a computer with much more than 32 Gb is required

# Learn parametesr of the noise classifier
LEARN_NOISE_CLASSIFIER.py -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  TEY_BEN_Summer TEY_BEN_Autumn TEY_BEN_Winter TEY_BEN_Spring TEY_PEL_Summer TEY_PEL_Autumn TEY_PEL_Winter TEY_PEL_Spring

# Type OTUs of the 32 samples
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  B13_BEN_Autumn  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  B13_BEN_Spring  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  B13_BEN_Summer  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  B13_BEN_Winter  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  B13_PEL_Autumn  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  B13_PEL_Spring  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  B13_PEL_Summer  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  B13_PEL_Winter  

TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  COM_BEN_Autumn  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  COM_BEN_Spring  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  COM_BEN_Summer  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  COM_BEN_Winter  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  COM_PEL_Autumn  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  COM_PEL_Spring  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  COM_PEL_Summer  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  COM_PEL_Winter  

TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  JAC_BEN_Autumn  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  JAC_BEN_Spring  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  JAC_BEN_Summer  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  JAC_BEN_Winter  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  JAC_PEL_Autumn  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  JAC_PEL_Spring  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  JAC_PEL_Summer  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  JAC_PEL_Winter  

TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  TEY_BEN_Autumn  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  TEY_BEN_Spring  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  TEY_BEN_Summer  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  TEY_BEN_Winter  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  TEY_PEL_Autumn  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  TEY_PEL_Spring  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  TEY_PEL_Summer  
TYPE_OTU.py  -d ../DATA_DIATOMS -o ../OUTPUT_DIATOMS  TEY_PEL_Winter  

