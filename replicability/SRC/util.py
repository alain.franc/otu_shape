import re
import os.path
import numpy as np
import matplotlib.pyplot as plt
from sklearn.neighbors import KernelDensity



# For a given sample, write the files containing dissimilarity arrays
# in a temporary file. This allows among other things to share the same order
# for the filesin Python and R for computing SBM lambdas
def atof(text):
    try:
        retval = float(text)
    except ValueError:
        retval = text
    return retval
def natural_keys(text):
    '''alist.sort(key=natural_keys) sorts in human order'''
    return [ atof(c) for c in re.split(r'[+-]?([0-9]+(?:[.][0-9]*)?|[.][0-9]+)', text) ]

def get_save_distance_files(sample_dir, distance_files_filename):
    """Define an order for OTUs dissimilarity arrays files.

    Arguments :
    sample : the directory containing all and only the OTUs dissimilarity arrays of a sample.
    distance_files_filename : name of the file containing the list of OTUs dissimilarity arrays files.
    Result : 
    Return the list of OTUs dissimilarity arrays and write the list on a file.
    """

    distance_files = sorted(os.listdir(sample_dir), key=natural_keys)
    with open(distance_files_filename, "w") as fp:
        for f in distance_files:
            fp.write("%s\n" % f)    
    return distance_files



def get_save_composed(sample_dir, distance_files, is_composed_filename, options):
    """ Define composed/single/not considered OTUs of a sample.

    For considered OTUs (with more than <sequences_min_nb> sequences),
    define variables for each OTU of a sample:

    Arguments :
    sample_dir : the directory containing all and only the OTUs dissimilarity arrays of a sample.
    distance_files : the list of OTUs dissimilarity arrays files.
    is_composed_filename : file were composed caracteristic is written.
    Return :
    Variables:
       . is_composed (1: composed, 0: single, -1: not considered),
       . min_degree (minimum degree of sequence nodes in the coresponding graph), 
       . degree20 (ratio of sequences nodes with a degree less than 0.20), 
       . size (number of sequences), 
       . theta (ratio between the number of missing edges in the graph associated
         over the total number of possible edges)
    and write is_composed in a file.
    """
    sample_name = os.path.basename(sample_dir)
    theta = np.empty(len(distance_files)); theta.fill(-1)
    min_degree = np.empty(len(distance_files)); min_degree.fill(-1)
    degree20 = np.empty(len(distance_files)); degree20.fill(-1)
    is_considered = np.empty(len(distance_files), dtype=bool); is_considered.fill(False)
    size = np.empty(len(distance_files)); size.fill(-1)
    i = 0 # number of considered OTUs in the sample
    for f in distance_files:
        D = np.genfromtxt(sample_dir +"/"+ f)
        if (D.shape[0]-1 >= options.sequences_min_nb) : # beware at sequences names
            is_considered[i] = True
            D = np.delete(D, (0), axis=0); D = np.delete(D, (0), axis=1) # sequences names
            np.fill_diagonal(D, 0) # Set diagonal to zero
            theta[i] = np.sum(D>options.gap)/(D.shape[0]*(D.shape[0] - 1));
            size[i] = D.shape[0]
            G = (D<=options.gap)
            degree = np.sum(G, axis=1 )/G.shape[0]
            min_degree[i] = np.min( degree )
            degree20[i] = np.sum( degree  < 0.20 ) / G.shape[1]
        i = i + 1   
    print("In sample " +sample_name+ " " +str(np.sum(is_considered))+ " OTUs have more than "
          +str(options.sequences_min_nb)+ " sequences.")

    # theta threshold from kernel density
    considered_theta = theta[is_considered]
    kde = KernelDensity(kernel='gaussian', bandwidth=options.kde_bandwidth).fit(considered_theta[:, None])
    theta_d = np.linspace(0, 1, 10000)
    logprob = kde.score_samples(theta_d[:, None])
    prob = np.exp(logprob)

    # search the valley after the first peak
    threshold_theta = 1  # value of theta if only one mode
    last_prob = prob[0]
    j = 1
    is_up = True
    while j < prob.size:
        if is_up and prob[j] >= last_prob:
            last_prob = prob[j]
            j=j+1
        else:
            if is_up == True: 
                last_prob = prob[j]
                j=j+1
                is_up = False
            else:
                if prob[j] <= last_prob:
                    last_prob = prob[j]
                    j=j+1
                else:
                    threshold_theta = (j-1) / prob.size
                    j = prob.size

    fig = plt.figure()
    plt.fill_between(theta_d, prob, alpha=0.5)
    plt.vlines(threshold_theta, linestyle='dotted', ymin=0, ymax=np.max(prob)) 
    plt.savefig( options.output_dir +"/"+ sample_name + "_theta.png", dpi = 300, bbox_inches = 'tight')
    plt.close(fig)

    is_composed = np.empty(len(distance_files)); is_composed.fill(-1)
    is_composed[np.where(theta > threshold_theta)[0]] = 1 # OTU composed
    is_composed[np.where((theta <= threshold_theta) & (theta >=0))[0]] = 0 # OTU single

    np.savetxt(is_composed_filename, is_composed, fmt='%i' )
    return [is_composed, min_degree, degree20, size, theta]




