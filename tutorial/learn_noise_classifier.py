#!/usr/bin/env python3
"""
This file is part of OTU shape project. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3.0 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.
"""

# Learn OTU noise classifier from a set of samples
#
# Define the parameters of the frontier between single OTU with / without noise.
#
# Minimum required options usage:  learn_noise_classifier.py -d <data_dir> -o <output_dir> <sample>
# See detailed usage with -h option.
# For an example, see the joined shell script RUN_DIATOMS_STUDY.py .
#
# Arguments:
# sample1 [... samplek] : a set of directory names, eah containing all OTUs dissimilarity matrices files of a sample.
#         No constraint on the name of these files.
#         Sequence names for rows and columns must be provided in text file.
# Result:
# The learn parameters are written in <output_dir>/<params_noise_classifier> file.
# Two other files are written <output_dir>/<params_noise_classifier>.txt
# (confusion matrix and mean AUC with 10 Cross Validation) and
# <output_dir>/<params_noise_classifier>.png (display decision frontier
# with classification of train cases) to check the quality of the classifier.


import os
import sys 
import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_curve
from sklearn.metrics import auc
from sklearn.model_selection import train_test_split


from optparse import OptionParser
import tempfile

import util

# INITIALIZE ###################################
src_dir = "."
################################################

usage = "usage: %prog [options] sample_name_1 [sample_name_n]"
parser = OptionParser(usage=usage)
parser.add_option("-d", "--data_dir", dest="data_dir",
                  help="directory of samples",
                  type="string")
parser.add_option("-o", "--output_dir", dest="output_dir",
                  help="directory to store results",
                  type="string")
parser.add_option("-c", "--noise_classifier", dest="noise_classifier_filename",
                  help="filename of the file containing the parameters of the noise classifier (default value: params_noise_classifier)",
                  type="string", default="params_noise_classifier")
parser.add_option("-n", "--sequences_min_nb", dest="sequences_min_nb",
                  help="required minimum number of sequences to type an OTU (default value: 20)",
                  type="int", default=20)
parser.add_option("-g", "--gap", dest="gap",
                  help="distance threshold to define OTUs (default value: 9)",
                  type="int", default=9)
parser.add_option("-b", "--kde_bandwidth", dest="kde_bandwidth",
                  help="bandwidth parameter to estimate theta density (default value: 0.05)",
                  type="float", default=0.05)

(options, args) = parser.parse_args()

 # Read sample to consider, given in command line as argument
samples_name = args
if not samples_name:
    print("Give a set of train samples names."); sys.exit()
if not os.path.isdir(options.output_dir):
    os.mkdir(options.output_dir)
if options.sequences_min_nb<1:
    print("Required minimum number of sequences to type an OTU must be up to 0."); sys.exit() 
if options.gap<1:
    print("Required minimum number of sequences to type an OTU must be up to 0."); sys.exit() 
if options.kde_bandwidth<=0:
    print("Required minimum number of sequences to type an OTU must be up to 0."); sys.exit() 
    

# Read samples to consider, given in command line as arguments
for sample_name in samples_name:
    if not os.path.isdir(options.data_dir+"/"+sample_name):
        print("Sample " + sample_name + " is not found."); sys.exit()

tmpdir = tempfile.TemporaryDirectory(); tmp_dir = tmpdir.name


# Define training set X (SBM lambda_intra_max, lambda_inter)
# and Y (1: with noise, 0: without noise)
Y = []; X = [] # the size of X and Y is unknown

#install blockmodels if not installed
os.system("Rscript " + "INSTALL_BLOCKMODELS.R")

# A loop over all samples in test-set T

print("\nLoop over all samples in test set for expert-based classification and the features.\n")
for sample_name in samples_name:
    os.mkdir(tmp_dir +"/"+ sample_name)
    #
    print("\nsample", sample_name)
    # 
    distance_files_filename = tmp_dir +"/"+ sample_name +"_distance_files.txt" 
    is_composed_filename = tmp_dir +"/"+ sample_name +"_is_composed.txt"
    
    distance_files = util.get_save_distance_files(options.data_dir+"/"+sample_name, distance_files_filename)
    print("types any OTU as composed/single.")
    val = util.get_save_composed(options.data_dir+"/"+sample_name, distance_files, is_composed_filename, options)
    is_composed = val[0]
    min_degree = val[1]
    cmd = "Rscript " + src_dir + "/" + "run_SBM.R  " + is_composed_filename + " " + distance_files_filename + " " + options.data_dir + "/" + sample_name + " " +   tmp_dir + "/" + sample_name + " >/dev/null" 
    print("Getting the features with Poisson SBM for single OTUs.")
    os.system(cmd)
    
    lamda_intra_max = np.empty(len(distance_files)); lamda_intra_max.fill(-1)
    lamda_inter = np.empty(len(distance_files)); lamda_inter.fill(-1)
    i = 0
    for f in distance_files:
        lamda_filename = tmp_dir +"/"+ sample_name +"/"+ f +".txt"
        if is_composed[i] == 0 and os.path.exists(lamda_filename): #single OTU with 2 SBM classes found
            lamda = np.genfromtxt(lamda_filename) 
            lamda_intra_max = np.max(np.diag(lamda))
            lamda_inter = lamda[0,1]
            # Expert classification based on min degree
            if min_degree[i] <= 0.20: 
                X.append( [lamda_intra_max, lamda_inter])
                Y = np.append(Y, 1)  # with noise
            elif min_degree[i] >= 0.70:
                X.append( [lamda_intra_max, lamda_inter])
                Y = np.append(Y, 0)   # without noise
        i = i+1
X = np.asarray(X)
print("\nAll features computed.\n")

print("Number of negative train example: "+str(np.sum(Y==0)))
print("Number of positive train example: "+str(np.sum(Y==1)))
# Test if enough case of each type
if np.sum(Y==0)<20:
    print("Not enough negative train sample, at least 20 required !"); sys.exit()
if np.sum(Y==1)<20:
    print("Not enough positive train sample, at least 20 required !"); sys.exit()

# Learn classifier
print("\nlearning the classifier")
clf = svm.SVC(kernel = 'linear')
clf.fit(X, Y)

# Equation for the line of the decision boundary of a linear model
# The line equation can be constructed as: y = w0 + w1 * x1 + w2 * x2 + ...
# Where w0 is obtained from intercept_, w1 onwards are found in coef_,
# and x1 and onwards are your features.
classifier = np.empty(3)
for (intercept, coef) in zip(clf.intercept_, clf.coef_): 
    s = "y = {0:.3f}".format(intercept)
    for (i, c) in enumerate(coef):
        s += " + {0:.3f} * x{1}".format(c, i)
        classifier = [intercept, coef[0], coef[1]]

    f =  open(options.output_dir+"/"+options.noise_classifier_filename, 'w')
    f.write(str(classifier[0]) + "\n")
    f.write(str(classifier[1]) + "\n")
    f.write(str(classifier[2]) + "\n")
    f.write("# noise classifier equation\n")
    f.write("# y = <line 1> + <line 2> * lambda_intra_max + <line 3> * lambda_inter\n")
    f.close
    
# Confusion matrix
print("Computation of the confusion matrix.")
y_pred = clf.predict(X)    
y_pred = y_pred.astype('int')
conf = confusion_matrix(Y, y_pred)

# Mean AUC with 10 Cross Validation
print("Computation of the Area Under Curve.")
np.random.seed(123)
CV = 10
AUC = np.zeros(CV)
for k in range(0,CV):
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.25) 
    clf = svm.SVC(kernel = 'linear')
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test) 
    y_pred = y_pred.astype('int')
    fpr, tpr, _ = roc_curve(y_test, y_pred, pos_label=clf.classes_[1])
    AUC[k] = auc(fpr, tpr)

# Write log file    
f =  open(options.output_dir+"/"+options.noise_classifier_filename+".txt", 'w')
f.write("Confusion matrix:\n" + str(conf) + "\n\n")
f.write("Mean AUC with 10 Cross Validation:\n" + str(np.mean(AUC))  + "\n")
f.close

# Save decision surface plot
def make_meshgrid(x, y, h=.02):
    """Define meshgrid."""
    x_min, x_max = x.min() - 1, x.max() + 1
    y_min, y_max = y.min() - 1, y.max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    return xx, yy

def plot_contours(ax, clf, xx, yy, **params):
    """Plot contour."""
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    out = ax.contourf(xx, yy, Z, **params)
    return out

fig, ax = plt.subplots()
X0, X1 = X[:, 0], X[:, 1]
xx, yy = make_meshgrid(X0, X1)
plot_contours(ax, clf, xx, yy, cmap=plt.cm.coolwarm, alpha=0.8)
ax.scatter(X0, X1, c=Y, cmap=plt.cm.coolwarm, s=20, edgecolors='k')
ax.set_ylabel('lamda_inter')
ax.set_xlabel('lamda_intra_max')
ax.set_title('Decision surface')
plt.savefig(options.output_dir+"/"+options.noise_classifier_filename+".png", dpi = 600, bbox_inches = 'tight')
plt.close(fig)

