#!/usr/bin/env python3

"""
This file is part of OTU shape project. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3.0 of the License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty
of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.
"""

# Type OTUs of a given sample
#
# Type each OTU of the sample that have some than <sequences_min_nb> sequences.
# Type OTU as (i) composed / single and (ii) for single OTU only as wih /without noise
#
# Minimum required options usage:  TYPE_OTU.py -d <data_dir> -o <output_dir> <sample>
# See detailed usage with -h option.
# For an example, see the joined shell script RUN_DIATOMS_STUDY.py .

# Argument:
# sample: a directory name <sample> containing all OTUs dissimilarity matrices files of a sample.
#         No constraint on the name of these files.
#         Sequence names for rows and columns must be provided in text file.
# Result:
# Types are written in <output_dir>/<sample>_type.txt file with the following columns:
#   OTU name,
#   OTU size (number of sequences),
#   is_composed (1 : yes, 0 : no, -1 : OTU not considered),
#   is_with_noise (1 : yes, 0 : no, -1 : OTU not considered),
#   theta (ratio between the number of missing edges in the graph associated
#          to an OTU over the total number of possible edges),
#   y (output of noise classifier),
#   degree20 (proportion of sequence with a degree less than 0.20).
# An image is also written in <output_dir>/<sample>_theta.png file. This is the estimated
# density of theta with identification of the threshold differencing composed and
# single OTUs.
# User can then check that the estimated density of theta is smooth. If it is not the case
# the parameter kde_bandwidth must be adjusted.


import sys
import os.path
import numpy as np

from optparse import OptionParser
import tempfile

import util

# INITIALIZE ###################################
src_dir = "."
################################################

usage = "usage: %prog [options] sample_name"
parser = OptionParser(usage=usage)
parser.add_option("-d", "--data_dir", dest="data_dir",
                  help="directory of samples",
                  type="string")
parser.add_option("-l", "--from_learning_dir", dest="from_learning_dir",
                  help="directory where to read the classifier",
                  type="string")
parser.add_option("-o", "--output_dir", dest="output_dir",
                  help="directory to store results",
                  type="string")                  
parser.add_option("-S", "--type_only_single", dest="type_only_single",
                  help="Type only composed/single and no noise (default value: False)",
                  action="store_true", default=False)
parser.add_option("-c", "--noise_classifier", dest="noise_classifier_filename",
                  help="filename of the file containing the parameters of the noise classifier (default value: params_noise_classifier)",
                  type="string", default="params_noise_classifier")
parser.add_option("-n", "--sequences_min_nb", dest="sequences_min_nb",
                  help="required minimum number of sequences to type an OTU (default value: 20)",
                  type="int", default=20)
parser.add_option("-g", "--gap", dest="gap",
                  help="distance threshold to define OTUs (default value: 9)",
                  type="int", default=9)
parser.add_option("-b", "--kde_bandwidth", dest="kde_bandwidth",
                  help="bandwidth parameter to estmate theta density (default value: 0.05)",
                  type="float", default=0.05)

(options, args) = parser.parse_args()

print("\ntyping sample", sys.argv[-1], "\n")

print("reading the classifier.")
if not os.path.isdir(options.output_dir):
    os.mkdir(options.output_dir)
if not options.type_only_single:
    if not os.path.exists(options.from_learning_dir + "/" + options.noise_classifier_filename):
        print("File with parameters of the noise classifier not found."); sys.exit()
    classifier = np.empty(3); 
    with open(options.from_learning_dir + "/" + options.noise_classifier_filename) as f:
        classifier[0] = float(f.readline())
        classifier[1] = float(f.readline())
        classifier[2] = float(f.readline())

print("checking the options")
if options.sequences_min_nb<1:
    print("Required minimum number of sequences to type an OTU must be up to 0."); sys.exit() 
if options.gap<1:
    print("Required minimum number of sequences to type an OTU must be up to 0."); sys.exit() 
if options.kde_bandwidth<=0:
    print("Required minimum number of sequences to type an OTU must be up to 0."); sys.exit() 

# Read sample to consider, given in command line as argument
sample_name = args
if not sample_name:
    print("Give a sample name."); sys.exit()
if len(sample_name) > 1:
    print("Just one sample at a time."); sys.exit()
sample_name = sample_name[0]
if not os.path.isdir(options.data_dir+"/"+sample_name):
    print("Sample " + sample_name + " not found."); sys.exit()    
         
tmpdir = tempfile.TemporaryDirectory(); tmp_dir = tmpdir.name
os.mkdir(tmp_dir +"/"+ sample_name)

distance_files_filename = tmp_dir +"/"+ sample_name + "_distance_files.txt" 
is_composed_filename = tmp_dir +"/"+ sample_name + "_is_composed.txt"
type_filename = options.output_dir  +"/"+ sample_name + "_type.txt"

print("type as composed/single and expertise for noise from degrees")
distance_files = util.get_save_distance_files(options.data_dir+"/"+sample_name, distance_files_filename)
val = util.get_save_composed(options.data_dir+"/"+sample_name, distance_files, is_composed_filename, options)
is_composed = val[0]
min_degree = val[1]
degree20 = val[2]
size = val[3]
theta = val[4]

y = np.empty(len(distance_files)); y.fill(-1)
is_noise = np.empty(len(distance_files)); is_noise.fill(-1)

if not options.type_only_single:
    print("computation of features")
    #os.system("Rscript " + src_dir+"/"+"run_SBM.R  " + is_composed_filename +" "+ distance_files_filename +" "+ 
    #           options.data_dir+"/"+sample_name + " " +   tmp_dir+"/"+ sample_name + " >/dev/null")
    os.system("Rscript " + src_dir+"/"+"run_SBM.R  " + is_composed_filename +" "+ distance_files_filename +" "+ options.data_dir+"/"+sample_name + " " +   tmp_dir+"/"+ sample_name)
    
    lamda_intra_max = np.empty(len(distance_files)); lamda_intra_max.fill(-1)
    lamda_inter = np.empty(len(distance_files)); lamda_inter.fill(-1)
    i = 0
    print("loop over dissimilarity arrays")
    for f in distance_files:
        #x = f.split(".")
        #print("typing", x[0], flush=True)
        lamda_filename = tmp_dir +"/"+ sample_name +"/"+ f +".txt"
        if is_composed[i] == 0:
            if not os.path.exists(lamda_filename):
                is_noise[i] = -1	# changé af 23.11.21 (instead of noise=0)
            else:
                lamda = np.genfromtxt(lamda_filename) 
                lamda_intra_max = np.max(np.diag(lamda))
                lamda_inter = lamda[0,1]
                y[i] = classifier[0] + classifier[1] * lamda_intra_max + classifier[2] * lamda_inter
                if y[i] > 0:
                    is_noise[i] = 1  # with noise
                else:
                    is_noise[i] = 0  # without noise
        i = i+1
 
print("output written in file", type_filename, "\n")    
with open(type_filename, 'w') as fp:
    fp.write("otu\t#sequences\tis_composed\tis_noisy\ttheta\ty\tdegree20\n")
    i = 0
    for f in distance_files:
        print("%s\t%i\t%i\t%i\t%1.4f\t%1.4f\t%1.4f" % (f, size[i], is_composed[i], is_noise[i], theta[i], y[i], degree20[i]) )
        fp.write("%s\t%i\t%i\t%i\t%1.4f\t%1.4f\t%1.4f\n" % (f, size[i], is_composed[i], is_noise[i], theta[i], y[i], degree20[i]) )
        i = i+1







